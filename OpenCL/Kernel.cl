__kernel void sum(__global int* pictureMatrix, __global int* outputSums, __global int* width, __global int* height){
	
	int workItemNum = get_global_id(0); 
	int workGroupNum = get_group_id(0); 
	int localGroupID = get_local_id(0);
	int w = *width;
	int h = *height;

	int res = 0;
	for (int i = 0; i < 3; i++)
		{res = res + pictureMatrix[(workItemNum*3)+i];}
	
	outputSums[workItemNum] = res;


	barrier(CLK_LOCAL_MEM_FENCE);
}

__kernel void NoiseR(__global int* pictureMatrix, __global int* outputSums, __global int* width, __global int* height){
	// Coming to use the noise reduction using sums calculated on the sum kernel function
}




