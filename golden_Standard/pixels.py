def sum(pixel):
    R = int(pixel[0])
    G = int(pixel[1])
    B = int(pixel[2])
    total = R + G + B
    return total

from statistics import median
from PIL import Image
from numpy import asarray
import numpy as np
  

img = Image.open('n.jpg')
numpydata = asarray(img)
row, col, rgb = numpydata.shape
PixelSum =  np.zeros((row, col))
Results = asarray(img)
print('dimension =',row,'x',col )

for x in range(0,row):
    for y in range(0,col):
        pixel = numpydata[x][y]
        PixelSum[x][y] = sum(pixel)

dim = 3
for x in range(0,row):
    for y in range(0,col):
        if (x>=dim and y >= dim and y<col-dim and x<row-dim):
            Surround = PixelSum[x-dim:x+dim+1, y-dim:y+dim+1]
            med = np.median(Surround)
            z = np.where(Surround == med)
            xr = int(z[0][0]) - dim
            yr = int(z[1][0]) - dim
            Results[x][y] = numpydata[x+xr][y+yr]

im = Image.fromarray(Results)
Image.fromarray(Results).save('Results.png')