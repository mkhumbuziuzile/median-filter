#include<stdio.h>
#include<CL/cl.h>
#include<iostream>
#include<fstream>
#include<string>
#include<cmath>
#include <tuple>
#include<iostream>
#include<fstream>
using namespace std;

int main(void)
{   int w = 2;
    int h = 3;
    int rgb = 3;

    int outputSums[w*h];
    int pictureMatrix[w*h*rgb];

    //Opening the file
    ifstream inputfile("pixels.txt");
    
    if (!inputfile.is_open()) 
    cout<<"Error opening file" ;

    //Defining the loop for getting input from the file
    
    for (int c = 0; c < w*h*rgb; c++) //Outer loop for rows
    {
          inputfile >> pictureMatrix[c];  //Take input from file and put into myArray
    }


	cl_mem pictureMatrix_buffer, outputSums_buffer, w_buffer, h_buffer;

    // Step 1
    //1.1 Get the platform available
	cl_uint platformCount; 
    clGetPlatformIDs(5, NULL, &platformCount); 
	cl_platform_id *platforms;
    platforms = (cl_platform_id*) malloc(sizeof(cl_platform_id) * platformCount);
	clGetPlatformIDs(platformCount, platforms, NULL);
    
    int i, j;
    char* info;
    size_t infoSize;
    const char* attributeNames[3] = { "Name", "Vendor", "Profile" };
    const cl_platform_info attributeTypes[5] = { CL_PLATFORM_NAME, CL_PLATFORM_VENDOR, CL_PLATFORM_PROFILE, };
    const int attributeCount = sizeof(attributeNames) / sizeof(char*);

    for (i = 0; i < platformCount; i++) {
        printf("\n Platform number: %d. \n", i);

        for (j = 0; j < attributeCount; j++) {

            // get platform attribute value size
            clGetPlatformInfo(platforms[i], attributeTypes[j], 0, NULL, &infoSize);
            info = (char*) malloc(infoSize);

            // get platform attribute value
            clGetPlatformInfo(platforms[i], attributeTypes[j], infoSize, info, NULL);

            printf("  %d.%d %-11s: %s\n", i+1, j+1, attributeNames[j], info);
            free(info);

        }
        printf("\n");

    }

    // 1.2. Choose platform you want to use 
    int p; 
    printf("Enter Platform Number to use: ");
    scanf("%d", &p);
	cl_platform_id platform = platforms[p];
    // -------------------------------------------------------------------------------------------------------------------------

    // Step 2
    // Set up device
    cl_device_id device;
    cl_uint num_device;
    int err;
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &num_device); //By default set device as GPU
	
    if (num_device > 0)
    {    //If there is a GPU for choosen platform, set up GPU
        printf ("Device setup will be GPU - Graphic Processing Unit\n");
        err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
    }
    else
    {   
        //If there are no GPU for choosen platform use CPU
        printf ("Device setup will be CPU - Central Processing Unit\n");
        err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &device, NULL);
    }
	
	cl_context context = clCreateContext(NULL, 1, &device, NULL, NULL, NULL);
	FILE * program_handle;
    program_handle = fopen("OpenCL/Kernel.cl", "r");
    size_t program_size; //, log_size;
	fseek(program_handle, 0, SEEK_END);
	program_size = ftell(program_handle);
	rewind(program_handle);

    char *program_buffer;//, *program_log;
	program_buffer = (char*)malloc(program_size + 1);
	program_buffer[program_size] = '\0';
	fread(program_buffer, sizeof(char), program_size, program_handle);
	fclose(program_handle);

	cl_program program = clCreateProgramWithSource(context, 1, (const char**)&program_buffer, &program_size, NULL);

	cl_int err3= clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    printf("program ID = %i\n", err3);

	cl_kernel kernel = clCreateKernel(program, "sum", &err);

	cl_command_queue queue = clCreateCommandQueueWithProperties(context, device, 0, NULL);
	size_t global_size = w * h; //total number of work items
    size_t local_size = w; //Size of each work group
	while (local_size>1000)
		{
			local_size = local_size/2;
		}
    cl_int num_groups = h; //number of work groups needed


    pictureMatrix_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, global_size*global_size*sizeof(int), pictureMatrix, &err);
    h_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), &h , &err);
    outputSums_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE| CL_MEM_COPY_HOST_PTR, global_size*global_size*sizeof(int), outputSums, &err);
	w_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), &w, &err);
	
	clSetKernelArg(kernel, 0, sizeof(cl_mem), &pictureMatrix_buffer);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), &outputSums_buffer);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), &w_buffer);
	clSetKernelArg(kernel, 3, sizeof(cl_mem), &h_buffer);

	cl_int errF =  clFinish(queue);
	cl_int err4 = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global_size, &local_size, 0, NULL, NULL);

	err = clEnqueueReadBuffer(queue, outputSums_buffer, CL_TRUE, 0,global_size*global_size*sizeof(int), outputSums, 0, NULL, NULL);

    printf("\nSum of RGB\n");
    for (int c = 0; c < h*w; c++)
    {
        cout << outputSums[c] << "\t";
    }

	clReleaseKernel(kernel);
	clReleaseMemObject(pictureMatrix_buffer);
	clReleaseMemObject(outputSums_buffer);
	clReleaseMemObject(w_buffer);
    clReleaseMemObject(h_buffer);
	clReleaseCommandQueue(queue);
	clReleaseProgram(program);
	clReleaseContext(context);

	return 0;
}